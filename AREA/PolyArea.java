import java.io.*;
import java.util.*;

class Vertex {
    double x;
    double y;

    public Vertex(double x, double y) {
	this.x = x;
	this.y = y;
    }

    public String toString() {
	return x+","+y;
    }
}

class Polygon {
    int nbVertices;
    Vertex[] vertices;

    public Polygon(int nbVertices) {
      vertices = new Vertex[nbVertices];
    }

    /* à compléter en s'insiprant de PolyPeri
     */
    public double getCote(Vertex v1, Vertex v2){
      return Math.sqrt(((v1.x-v2.x)*(v1.x-v2.x)) + ((v1.y-v2.y)*(v1.y-v2.y)));
    }

}

class PolyArea {

    public static void main(String[] args) {
	Locale.setDefault(Locale.ENGLISH);

	Scanner scan = new Scanner(System.in);
	int nbPoly = scan.nextInt();
	for(int i=0;i<nbPoly;i++) {

	    /* à compléter en s'insiprant de PolyPeri
	     */
      int nbVertices = scan.nextInt();
      Polygon p = new Polygon(nbVertices+1);

    for(int j = 0; j<nbVertices; j++){
      Vertex v = new Vertex(scan.nextFloat(),scan.nextFloat());
      p.vertices[j] = v;
    }
    p.vertices[nbVertices] = p.vertices[0];
    double sum1 = 0;
    for (int k = 0; k<nbVertices ; k++ ) {
      sum1 += p.vertices[k].x*p.vertices[k+1].y;
    }

    double sum2 = 0;
    for (int l = 1; l<nbVertices+1 ; l++ ) {
      sum2 += p.vertices[l-1].y*p.vertices[l].x;
    }
    double total = sum2 - sum1;

    System.out.print(nbVertices+" ");
    System.out.println(Math.abs(total/2));

    }
	}

}
