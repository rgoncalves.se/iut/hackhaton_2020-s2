import java.io.*;
import java.util.*;


/*
  Pour tester :
  
  01_5conv.in est un fichier d'entrée de test.
  01_5conv.ans est le fichier des réponses correspondantes.

  
  java Babase <01_5conv.in >res.txt
  diff res.txt 01_5conv.ans

  Si la commande diff n'indique rien, cela veut dire que votre
  programme a bien calculé ce qu'il faut. Vous pouvez essayer de le
  soumettre.

 */



class BaseConverter {
    
    public static String inverseString(String chaine){
    String result="";
    int i;
    for (i=chaine.length()-1;i>=0;i--){
        result.concat(""+chaine.charAt(i));
        System.out.println(result);
    }
        return result;
    }
   
    public BaseConverter() {
    
    }

    public String convert(String n, int bIn, int bOut) {
	   
	// convert n in base bIn into base bOut
        int r=0;
        int p=0;
        String ni=inverseString(n);
        for (int i=0;i<ni.length();i++){
            r +=i*(Math.pow(bIn,p));
            p=p+1;
        }
        int b10=r;
        String resultat="";
        while (r>0){
            resultat= "" + r%bOut+resultat;
            r=r/bOut;
        }
        return resultat;

	/* a completer */
	
	      
    }
}

class Babase {

    public static void main(String[] args) {
	Locale.setDefault(Locale.ENGLISH);

	try {
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    String line = "";
	    
	    line = br.readLine();
	    
	    int nb = Integer.parseInt(line);
	    int bIn,bOut;
	    String nombre;
	    BaseConverter conv = new BaseConverter();
	    
	    for(int i=0;i<nb;i++) {
		line = br.readLine();
		String[] lst = line.split(" ");
		nombre = lst[0];
		bIn = Integer.parseInt(lst[1]);
		bOut = Integer.parseInt(lst[2]);
		
		System.out.println(conv.convert(nombre, bIn, bOut));
	    }
	}
	catch(IOException e) {}
    }
}
