def bintodec(chaine):
    j=0
    resu=0
    chaine_inverse=chaine[::-1]
    for i in chaine_inverse:
        resu+=int(i)*(2**j)
        j += 1
    return resu

#print bintodec("1010100")



def dectobin(n):
    chaine=""
    while n>0:
        chaine = str(n%2) + chaine
        n=n/2
    return chaine
#print dectobin(84)

def bintohexa(chainebin):

    Hexa=["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"]
    BIN=["0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"]
    chaineHexa=""
    while (len(str(chainebin))%4!=0):
        chainebin="0" + str(chainebin)
    while (chainebin!=''):
        minichaine=str(chainebin)[0:4]
        chaineHexa += Hexa[BIN.index(minichaine)]
        chainebin=str(chainebin)[4:]
    return chaineHexa
#print bintohexa(10001000111111011010110101101011110)

def hexatobin(chaineHexa):
    Hexa=["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"]
    BIN=["0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"]
    chainebin=""
    while(chaineHexa!=""):
        minichaine=chaineHexa[0:1]
        chainebin+=BIN[Hexa.index(minichaine)]
        chaineHexa=chainebin[1:]
    return chainebin    

#print hexatobin("1F")

def bases(chaine,base_dep,base_arr):

    r=0
    p=0
    Hexa=["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"]
    chaine_inverse=chaine[::-1]
    for i in chaine_inverse:
        r+=int(i)*(base_dep**p)
        p=p+1
    b10=r
    resultat=""
    while r>0:
        resultat= str(r%base_arr)+resultat
        r=r/base_arr
    return resultat
print bases('8760',9,4)