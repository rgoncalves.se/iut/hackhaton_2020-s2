import java.io.*;
import java.util.*;


class SnakePath {

    public SnakePath() {
    }

    public String head(int time) {
	int row = 0,col=0;
  boolean cpt = false;
  boolean qui = false;
  boolean monte = true;

	/* a completer */
  for (int i = 0; i<time ; i++ ) {
      if (col == 0){
        if(cpt){
          col += 1;
          cpt = false;
          monte = true;
        }
        else{
          row += 1;
          cpt = true;
        }
      }
      else if (row == 0){
        if(cpt){
          row += 1;
          cpt = false;
          monte = true;
        }
        else{
          col += 1;
          cpt = true;
        }
      }
      else if(row == col){
        if (!qui){
          row -= 1;
          qui = true;
        }
        else{
          col -= 1;
          qui = false;
        }
        monte = false;
      }
      else if(qui){
        if(monte){
          row += 1;
        }
        else{
          row -= 1;
        }
      }
      else if(!qui){
        if (monte){
          col += 1;
        }
        else{
          col -= 1;
       }
      }
    }

	return row+","+col;
    }

}

class Snake {

    public static void main(String[] args) {
	Locale.setDefault(Locale.ENGLISH);

	try {
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    String line = "";

	    line = br.readLine();

	    int nb = Integer.parseInt(line);
	    SnakePath snake = new SnakePath();

	    for(int i=0;i<nb;i++) {
		line = br.readLine();
		int t = Integer.parseInt(line);
		System.out.println(snake.head(t));
	    }
	}
	catch(IOException e) {}
    }
}
