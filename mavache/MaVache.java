import java.io.*;
import java.util.*;

/* NOTE:

   L'article de cours-info qui permet de télécharger ce code contient
   également un lien vers un fichier testdata.in qui permet de lancer un
   test simple avec votre programme.

   Pour cela, il suffit de compiler votre programme puis de lancer :
   java MaVache < testdata.in

   Si vous constatez que le résultat affiché correspond à ce qui est
   donné dans l'énoncé, votre programme est peut être juste et vous
   pouvez le soumettre. Sinon, n'essayez même pas :-)
   */

class Cloture {

	int polyCorners; // nb de poteaux
	double sumX;
	double sumY;
	double[] polyX; // les coordonnées en x de chaque poteau
	double[] polyY; // les coordonnées en y de chaque poteau
	double[] trash;

	// autres attributs ???

	public Cloture() {
		// ???
	}

	/* readParameters():
	   permet de lire avec scan les paramètres de la cloture :
	   le nombre de poteaux suivi des coordonnées x et y de chacun
	   */
	public void readParameters(Scanner input) {

		String[] trash;
		String tmp;
		int j = 0;
		int len;
		input.useDelimiter(System.getProperty("line.separator"));

		tmp = input.next();
		System.out.println(" === ");
		System.out.println(tmp);
		trash = tmp.split(" ");

		len = (trash.length-1) / 2;
		this.polyX = new double [ len ];
		this.polyY = new double [ len ];

		this.polyCorners = Integer.valueOf(trash[0]);
		for(int i = 1; i < trash.length; i+=2) {
			this.polyX[j] = Double.valueOf(trash[i]);
			this.polyY[j] = Double.valueOf(trash[i+1]);
			j++;
		}
	}

	public void calcGravity() {
		sumX = 0;
		sumY = 0;
		for(int i = 0; i < this.polyX.length; i++) {
			sumX += this.polyX[i];
			sumY += this.polyY[i];
		}
		sumX = 1./this.polyX.length * sumX;
		sumY = 1./this.polyX.length * sumX;
	}

	public void isInside() {
		if(this.sumX < getMin(this.polyX) || this.sumX > getMax(this.polyX) || this.sumY < getMin(this.polyY) || this .sumY > getMax(this.polyY)) {
			System.out.println(this.polyCorners + " no");
		}
		else {
			System.out.println(this.polyCorners + " yes");
		}
	}

	public void inPolygon() {
		int j = polyCorners - 1;
		double[] constant = new double [ polyY.length ];
		double[] multiple = new double [ polyY.length ];

		for(int i=0; i<polyCorners; i++) {  
			if(polyY[j]==polyY[i]) {
				constant[i]=polyX[i];
				multiple[i]=0;
			}
			else {
				constant[i]=polyX[i]-(polyY[i]*polyX[j])/(polyY[j]-polyY[i])+(polyY[i]*polyX[i])/(polyY[j]-polyY[i]);
				multiple[i]=(polyX[j]-polyX[i])/(polyY[j]-polyY[i]);
			}
			j=i;
		}

		j = polyCorners-1 ;
		boolean oddNodes = false;

		for (int i=0; i<polyCorners; i++) {
			if ((polyY[i]< this.sumY && polyY[j]>=this.sumY || polyY[j]< this.sumY && polyY[i]>= this.sumY)) {
				oddNodes ^= (this.sumY*multiple[i]+constant[i]< this.sumX);
			}
			j = i;
		}

		System.out.println(oddNodes);


	/*
	   for(int i = 0; i < this.polyCorners; i++) {
	   if(this.polyY[][i] < this.sumY && this.polyY[][j] >= this.sumY || this.polyY[][j] < this.sumY && this.polyY[][i] >= this.sumY) {
	   System.out.println("C");
	   if(this.polyX[][i] + (this.sumY - this.polyY[][i]) / (this.polyY[][j] - this.polyY[][i]) * (this.polyX[][j] - this.polyX[][i]) < this.sumX) {
	   System.out.println("CC");
	   insins = !insins;
	   }
	   }
	   j = i;
	   }
	   System.out.println(insins);
	   */
}

public double getMax(double[] tab) {
	double tmp = tab[0];
	for(int i = 1; i < tab.length; i++) {
		if(tab[i] > tab[0])
			tmp = tab[i];
	}
	return tmp;
}
public double getMin(double[] tab) {
	double tmp = tab[0];
	for(int i = 1; i < tab.length; i++) {
		if(tab[i] < tab[0])
			tmp = tab[i];
	}
	return tmp;
}

public void showTab(double[] tab) {
	for(int i = 0; i < tab.length; i++) {
		System.out.println(tab[i]);
	}
}
public void showTab(String[] tab) {
	for(int i = 0; i < tab.length; i++) {
		System.out.println(tab[i]);
	}
}

}


class MaVache {

	public static void main(String[] args) {
		Locale.setDefault(Locale.ENGLISH);

		Scanner scan = new Scanner(System.in);
		int polyCorners = scan.nextInt();

		for(int i = 0; i < polyCorners; i++) {

			Cloture c = new Cloture();
			c.readParameters(scan);
			c.calcGravity();
			c.isInside();
			c.inPolygon();

			/* A COMPLETER : (cf. sujet)

			   afficher sur la sortie standard le nombre
			   de poteau de la cloture puis yes ou no selon que la vache, placée
			   au centre de la cloture va être ou non à l'intérieur de la cloture.
			   */
		}
	}
}
