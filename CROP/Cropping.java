import java.io.*;
import java.util.*;

class Image {

    byte[][] pixels;
    int width;
    int height;

    public Image(int width, int height) {
	this.width = width;
	this.height = height;
	pixels = new byte[height][width];
    }

    public void readPixels(Scanner scan) {
      for (int i = 0; i<this.height ; i++ ) {
        String stringBuffer = scan.next();
        for (int j = 0; j<this.width ; j++ ) {
          this.pixels[i][j] = (byte)stringBuffer.charAt(j);
          this.pixels[i][j] -= 48;
          // System.out.print(this.pixels[i][j]);
        }
        // System.out.println();
      }
    }

    public void printBoundingBox() {

	int boxUp,boxLeft;
	int boxWidth,boxHeight;
	/* A COMPLETER :

	   calcule  les coordonnées du
	   coin supérieur haut de la boite englobante, ainsi que sa
	   largeur et hauteur (cf. énoncé)

	 */
   boxUp = height;
   boxLeft = width;
   int maxUp = 0;
   int maxLeft = 0;

   for (int i = 0; i<this.height ; i++ ) {
     for (int j = 0; j<this.width ; j++) {
       if (this.pixels[i][j] == 1){
         if (boxUp > i) {
           boxUp = i;
         }
         if (boxLeft > j) {
           boxLeft = j;
         }
         if (maxUp < i) {
           maxUp = i;
         }
         if (maxLeft < j) {
           maxLeft = j;
         }
       }
     }
   }

   boxWidth = maxLeft - boxLeft +1 ;
   boxHeight = maxUp - boxUp +1 ;
   // System.out.println(boxLeft+" "+boxUp);

	System.out.println(boxLeft+" "+boxUp+" "+boxWidth+" "+boxHeight);
    }
}


class Cropping {

    public static void main(String[] args) {

	Locale.setDefault(Locale.ENGLISH);

	Scanner scan = new Scanner(System.in);

	/* A COMPLETER :
	   - lire la largeur et la hauteur de l'image
	   - instancier l'image
	   - lire les pixels de l'image (cf. readPixels())
	   - afficher les paramètres de la boite englobante
	 */
    int largeur = scan.nextInt();
    int hauteur = scan.nextInt();
    // System.out.println(largeur);
    // System.out.println(hauteur);
    Image i = new Image(largeur,hauteur);
    i.readPixels(scan);
    i.printBoundingBox();
    }
}
