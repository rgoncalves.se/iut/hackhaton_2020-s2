import java.io.*;
import java.util.*;
import java.math.*;

class Finder {

    public Finder() {
    }

    public String find(int min, int max) {
	String ret="";

	/* a completer:

	   IMPORTANT: vu la longueur des nombres qui sont manipulés il
	   est impossible d'utiliser des int, long. Les double ne sont
	   pas utilisables non plus car le résultat d'un modulo entre
	   un double et un int peut ne pas être correct.

	   La seule solution est d'utiliser la classe BigInteger, qui
	   permet de faire des calculs sur des entiers de longueurs
	   arbitraire, notamment un modulo.

	*/

  BigInteger ten = BigInteger.valueOf(10);
  BigInteger d = BigInteger.valueOf(1);
  BigInteger one = BigInteger.valueOf(1);
  BigInteger nine = BigInteger.valueOf(9);
  BigInteger zero = BigInteger.valueOf(0);

  for (int i = 0; i<min-1 ; i++ ) {
    d = d.multiply(ten);
  }

   for (int i = min; i<max ; i++ ) {
     while(d.mod(BigInteger.valueOf(i)) != zero){
       if (d.mod(ten.pow(i)) != nine) {
         d = d.add(one);
         // System.out.println(d);
       }
       else return "-1";
     }
     d = d.multiply(ten);
   }
  ret = String.valueOf(d);
	return ret;
    }

}


class Superdiv {

    public static void main(String[] args) {
	Locale.setDefault(Locale.ENGLISH);

	try {
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    String line = "";

	    line = br.readLine();

	    int nb = Integer.parseInt(line);
	    int val;
	    Finder f = new Finder();

	    for(int i=0;i<nb;i++) {
		line = br.readLine();
		String[] lst = line.split(" ");
		int min = Integer.parseInt(lst[0]);
		int max = Integer.parseInt(lst[1]);
		System.out.println(f.find(min,max));
	    }
	}
	catch(IOException e) {}
    }
}
