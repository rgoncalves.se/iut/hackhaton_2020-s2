import java.io.*;
import java.util.*;


class BaseConverter {
   
    public BaseConverter() {
    }

    public String convert(int v) {
	if(v<0)
		return flip(convert(-v));
	if(v==0)
		return "";
	int rem=mod3(v);
	if(rem==0)
		return convert(v/3)+"0";
	if(rem==1)
		return convert(v/3)+"1";
	if(rem==2)
		return convert((v+1)/3)+"-1";
	
	return "res";
	}
	private String flip(String s)
		{
			String flip="";
			for(int i=0;i<s.length();i++)
			{
				if(s.charAt(i)=='1')
					flip+="-1";
				else if(s.charAt(i)=='1' && s.charAt(i+1)=='-')
					flip+='1';
				else
					flip+='0';
			}
			return flip;
		}
		private int mod3(int v)
		{
			if(v>0)
				return v%3;
			v=v%3;
			return (v+3)%3;
		}
}

class B3C {

    public static void main(String[] args) {
	Locale.setDefault(Locale.ENGLISH);

	try {
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    String line = "";
	    
	    line = br.readLine();
	    
	    int nb = Integer.parseInt(line);
	    int val;
	    BaseConverter conv = new BaseConverter();
	    
	    for(int i=0;i<nb;i++) {
		line = br.readLine();	
		if (Integer.parseInt(line)==0){
			System.out.println("0");
		}else{
			System.out.println(conv.convert(Integer.parseInt(line)));
		}	
	    }
	}
	catch(IOException e) {}
    }
}