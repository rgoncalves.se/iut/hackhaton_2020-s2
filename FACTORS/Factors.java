import java.util.*;

class Factor {

    public static void main(String[] args) {

	Locale.setDefault(Locale.ENGLISH);
        Scanner scan = new Scanner(System.in);

        // Lit le nombre de lignes
        int n = scan.nextInt();

	for(int i=0;i<n;i++) {
	    int num = scan.nextInt();
	    // fait la décomposition et affiche le résultat
      int d = 3;
      while (num%2 == 0){
        num/=2;
        System.out.print(2+" ");
      }
      if(num == 1){
      }
      else{
        while (d <= Math.sqrt(num)){
          while (num % d == 0){
            num/= d;
            System.out.print(d+" ");
          }
          d += 2;
        }
        if (num != 1){System.out.print(num);}
      }
      System.out.println();

	}
    }
}

// def decompose(n):
//     Liste=[]
//     i=3
//
//     if n < 2:
//         return "ERROR: n < 2"
//     while n%2 == 0:
//         n/=2
//         Liste.append(2)
//     if n==1:
//         return Liste
//     while i<= sqrt(n):
//         while n%i == 0:
//             n/=i
//             Liste.append(i)
//         i+=2
//     Liste.append(n)
//     return Liste
