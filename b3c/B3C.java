import java.io.*;
import java.util.*;


class BaseConverter {

	public BaseConverter() {
	}

	public String convert(int val) {
		final int SAVE = val;
		String res = "";
		int rem;
		int i = 0;

		while(val > 0) {
			rem = val%3;
			val = val/3;
			res = rem + res;
			i += 3;
		}

		System.out.println(SAVE + " " + res);

		return res;
	}
}

class B3C {

	public static void main(String[] args) {
		Locale.setDefault(Locale.ENGLISH);

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String line = "";

			line = br.readLine();

			int nb = Integer.parseInt(line);
			int val;
			BaseConverter conv = new BaseConverter();

			for(int i=0;i<nb;i++) {
				line = br.readLine();		
				System.out.println(conv.convert(Integer.parseInt(line)));
			}
		}
		catch(IOException e) {}
	}
}
